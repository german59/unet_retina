#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__=["Abraham Sanchez"]
__copyright__="Copyright 2019, Gobierno de Jalisco"
__credits__=["Abraham Sanchez"]
__license__="MIT"
__version__="0.0.1"
__maintainer__=["Abraham Sanchez"]
__email__="abraham.sanchez@jalisco.gob.mx"
__status__="Development"

'''
UNet network based on zhixuhao's model:
https://github.com/zhixuhao/unet/blob/master/model.py
'''

import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
from keras.models import Model
from keras.layers import Input
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import UpSampling2D
from keras.layers import Dropout
from keras.layers import concatenate
from keras import optimizers
from keras.callbacks import ModelCheckpoint


def unet(input_size=(256, 256, 3), opttype=0, lr=.001):
    inputs=Input(input_size)
    conv1=Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(inputs)
    conv1=Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv1)
    pool1=MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2=Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
    conv2=Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
    pool2=MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3=Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
    conv3=Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
    pool3=MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4=Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool3)
    conv4=Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
    drop4=Dropout(0.5)(conv4)
    pool4=MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5=Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool4)
    conv5=Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
    drop5=Dropout(0.5)(conv5)

    up6=Conv2D(512, 2, activation='relu', padding='same', kernel_initializer='he_normal')(UpSampling2D(size=(2,2))(drop5))
    merge6=concatenate([drop4,up6], axis=3)
    conv6=Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge6)
    conv6=Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv6)

    up7=Conv2D(256, 2, activation='relu', padding='same', kernel_initializer='he_normal')(UpSampling2D(size=(2,2))(conv6))
    merge7=concatenate([conv3,up7], axis=3)
    conv7=Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge7)
    conv7=Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv7)

    up8=Conv2D(128, 2, activation='relu', padding='same', kernel_initializer='he_normal')(UpSampling2D(size=(2,2))(conv7))
    merge8=concatenate([conv2,up8], axis=3)
    conv8=Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge8)
    conv8=Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)

    up9=Conv2D(64, 2, activation='relu', padding='same', kernel_initializer='he_normal')(UpSampling2D(size=(2,2))(conv8))
    merge9=concatenate([conv1,up9], axis=3)
    conv9=Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge9)
    conv9=Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv9=Conv2D(2, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv10=Conv2D(1, 1, activation='sigmoid')(conv9)

    model=Model(inputs=inputs, outputs=conv10)

    optim={
        0: optimizers.SGD(lr=lr),
        1: optimizers.Adam(lr=lr),
        2: optimizers.RMSprop(lr=lr)
    }
    try:
        opt=optim[opttype]
    except:
        opt=optim[0] # set SGD as default
    
    model.compile(optimizer=opt, loss='binary_crossentropy', metrics=['accuracy'])
    print(model.summary())
    print('Optimizer:', opt)

    return model

