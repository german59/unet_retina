#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = ["Abraham Sanchez"]
__copyright__ = "Copyright 2019, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import argparse

from unet_tiny import unet
from dataset import create_data
from keras.callbacks import ModelCheckpoint

parser = argparse.ArgumentParser(description='Image Segmentation')
parser.add_argument('-i', '--images', required=True, help='Images directory name.')
parser.add_argument('-m', '--masks', required=True, help='Masks directory name.')
parser.add_argument('-l', '--size', required=True, type=int, help='Input image size.')
parser.add_argument('-v', '--split', required=True, type=float, help='Train split size.')
parser.add_argument('-b', '--batchsize', required=True, type=int, help='Batch size.')
parser.add_argument('-e', '--epochs', required=True, type=int, help='Epochs.')
parser.add_argument('-o', '--optimizer', required=True, type=int, help='Optimizer (0) SGD, (1) Adam, (2) RMSprop.')
parser.add_argument('-t', '--lr', required=True, type=float, help='Learning rate.')
parser.add_argument('-n', '--name', required=True, help='Model file name.')
args = parser.parse_args()

# Create and split the dataset
x_train, x_test, y_train, y_test = create_data(args.images,
                                               args.masks,
                                               args.size,
                                               args.split)

print('Images train:', x_train.shape)
print('Masks train:', y_train.shape)
print('Images test:', x_test.shape)
print('Masks test:', y_test.shape)

# Create UNet model
model = unet(input_size=x_train[0].shape,
             opttype=args.optimizer,
             lr=args.lr)

checkpointer = ModelCheckpoint(filepath=args.name+'.hdf5',
                               verbose=1,
                               save_best_only=True)

# Train and evaluate the model
model.fit(x_train, y_train,
          batch_size=args.batchsize,
          epochs=args.epochs,
          verbose=1,
          callbacks=[checkpointer],
          validation_data=(x_test, y_test))

# Evaluation
print(model.evaluate(x_test, y_test, batch_size=args.batchsize))

