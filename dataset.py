#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = ["Abraham Sanchez"]
__copyright__ = "Copyright 2019, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import os
import gc
import numpy as np

from PIL import Image
from os.path import join
from sklearn.model_selection import train_test_split


def create_data(image_path, mask_path, img_size, train_size=.7):
    '''
    Create segmentation the dataset for train and validation.

    args:
        image_path, srt. Directory name of the images
        mask_path, srt. Directory name of the masks
        img_size, int. Image size. The image will be resized as (img_size, img_size).
        train_size, float. Proportion of the dataset to include in the train split (between 0.0 and 1.0).
    return:
        Four numpy arrays that contain the train, test data and its masks.
        Train images size: (Train_split, img_size, img_size, 1)
        Train masks size: (Train_split, img_size, img_size, 1)
        Test images size: (Test_split, img_size, img_size, 1)
        Test masks size: (Test_split, img_size, img_size, 1)
        
        Where Train_split is the proportion of the dataset to include in the train split.
    '''
    images = []
    masks = []
    size = (img_size, img_size)
    folders = [image_path, mask_path]
    ids = sorted(os.listdir(folders[0]))
    for id_image in ids:
        image = Image.open(join(folders[0], id_image)).convert('L')
        image = image.resize(size)
        image = np.array(image)
        image = np.expand_dims(image, axis=-1)

        mask = Image.open(join(folders[1], id_image)).convert('L')
        mask = mask.resize(size)
        mask = np.array(mask)
        mask = np.expand_dims(mask, axis=-1)
        mask = np.where(mask == 0, 0, 255)

        image = image / 255.
        mask = mask / 255.

        images.append(image)
        masks.append(mask)

    return train_test_split(np.array(images), np.array(masks), train_size=train_size)

