# Segmentador con Red Neuronal UNet

UNet es una red convolucional basada en autoencoders diseñada para realizar segmentación de imágenes médicas.

| Arquitectura UNet   |
|---|
| ![](https://gitlab.com/inteligencia-gubernamental-jalisco/segmentador-semantico/uploads/c96df317aeceb3c8db8a333b54ea63da/unet.png) |

*Para más información hacerca de UNet visitar el artículo [U-Net: Convolutional Networks for BiomedicalImage Segmentation](https://arxiv.org/pdf/1505.04597.pdf).*

El segmentador se compone de los siguientes archivos:

* `dataset.py`. Crea los datos que serán usados durante el entrenamiento y validación. Crea una relación de datos **Imagen-Mascara**.
* `unet.py`. Red UNet.
* `segmenter.py`. Código principal de ejecución. Llama a la creación de los datos y la red. Realiza el entrenamiento y guardado del modelo.

## Requirimientos

Python 3.6.7

 - TensorFlow (>=1.13.1)
 - Keras (>=2.2.4)
 - Pillow (>=6.0.0)
 - scikit-learn (>=0.20.3)

## Uso

```
usage: segmenter.py [-h] -i IMAGES -m MASKS -l SIZE -v SPLIT -b BATCHSIZE -e
                    EPOCHS -o OPTIMIZER -t LR -n NAME

Image Segmentation

optional arguments:
  -h, --help            show this help message and exit
  -i IMAGES, --images IMAGES
                        Images directory name.
  -m MASKS, --masks MASKS
                        Masks directory name.
  -l SIZE, --size SIZE  Input image size.
  -v SPLIT, --split SPLIT
                        Train split size.
  -b BATCHSIZE, --batchsize BATCHSIZE
                        Batch size.
  -e EPOCHS, --epochs EPOCHS
                        Epochs.
  -o OPTIMIZER, --optimizer OPTIMIZER
                        Optimizer (0) SGD, (1) Adam, (2) RMSprop.
  -t LR, --lr LR        Learning rate.
  -n NAME, --name NAME  Model file name.
```

### Ejemplo

Se tiene una carpeta de nombre *dataset* la cual tiene dos subcarpetas de nombre *Images* y *Masks*.

```
python3 segmenter.py -i dataset/Images/ -m dataset/Masks/ -l 128 -v 0.7 -b 1 -e 5 -o 1 -t 0.0001 -n test
```

Nota: Algunos datos de prueba se encontrar en el bucker gs://dai-2019/object_detection/ con el nombre **vessel_retina.tar.gz** y **crop_masks_retina.tar.gz**.

## Entrada

Imagenes en formato jpg, jpeg, png en cualquier resolución. Las imagenes deberán estar contenidas en una carpeta separada así como las mascaras en otra carpeta. Para cada archivo existente en la carpeta de imagenes debera existir otro archivo con el mismo nombre en la carpete de mascaras, por ejemplo:

```
Images/
    img0.jpg
    img1.jpg
    ...
    imgN.jpg
Masks/
    img0.jpg
    img1.jpg
    ...
    imgN.jpg
```

## Salida

Modelo del entrenamiento con nombre especificado en los parámetros de la aplicación.

